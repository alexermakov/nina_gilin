jQuery(document).ready(function ($) {

    if (!$("body.home").length) {
        $('.js_animate__header').removeClass('animate__header js_animate__header')
    }
    $('.js_title_animate').each(function () {
        $(this).html(`<span>${$(this).text()}</span>`)
    })
    $('.js_animate__image__view').each(function () {
        $(this).append(`<div class='animate__move__white_line js_animate__move__white_line'></div>`)
    })


    function animateAfterLoaded(timeStartScreenAnimation) {
        $('body').addClass('loaded')


        setTimeout(() => {

            setTimeout(() => {
                $(".js_animate_home_promo__decore__image--main").each(function () {
                    el = $(this)
                    elParent = $(this).parent()[0];

                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 85%',
                        },
                        y: '0',
                        opacity: 1,
                        scale: 1,
                        ease: "ease-in-out",
                        stagger: {
                            amount: 1
                        },
                        duration: 1.25

                    })
                })
            }, 0)

            setTimeout(() => {
                $(".js_animate_home_promo__decore__image--add").each(function () {
                    el = $(this)
                    elParent = $(this).parent()[0];

                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 85%',
                        },
                        y: '0',
                        opacity: 1,
                        scale: 1,
                        ease: "ease-in-out",
                        stagger: {
                            amount: 1
                        },
                        duration: 1.5

                    })
                })
            }, 800)


            $(".js_animate__home_promo__title").each(function () {
                el = $(this).find('span')
                elParent = $(this).parent()[0];

                gsap.to(el, {
                    scrollTrigger: {
                        trigger: elParent,
                        start: 'top 85%',
                    },
                    y: '0',
                    ease: "ease-in-out",
                    stagger: {
                        amount: 0.3
                    },
                    duration: 1

                })
            })

            $("body.home .js_animate__header").each(function () {
                el = $(this)
                elParent = $(this).parent()[0];

                gsap.to(el, {
                    scrollTrigger: {
                        trigger: elParent,
                        start: 'top 85%',
                    },
                    y: '0',
                    ease: "ease-in-out",
                    stagger: {
                        amount: 0.3
                    },
                    duration: 0.8

                })
            })

            setTimeout(() => {
                $(".js_animate__home_promo__info").each(function () {
                    el = $(this)
                    elParent = $(this).parent()[0];
                    gsap.to(el, {
                        scrollTrigger: {
                            trigger: elParent,
                            start: 'top 85%',
                        },
                        opacity: 1,
                        y: '0',
                        ease: "ease-in-out",
                        stagger: {
                            amount: 0.3
                        },
                        duration: 1

                    })
                })
            }, 500)


        }, timeStartScreenAnimation);
    }

    function loadedPreloader() {
        const allImages = document.querySelectorAll('img');
        const allImagesCount = allImages.length;
        let countImageLoaded = 0;

        localStorage.setItem('preloader', 1);

        function removeLoader() {
            setTimeout(() => {
                $('.js_loader_x').addClass('start_show')
                animateAfterLoaded(1000);

            }, 500);



        }

        const load = url => new Promise(resolve => {
            const img = new Image()
            img.onload = () => resolve({
                url
            })
            img.src = url;
            countImageLoaded++;
        });




        allImages.forEach(image => {
            (async () => {
                const {
                    url
                } = await load(image.src);
            })();
            if (allImagesCount == countImageLoaded) {
                setTimeout(() => {
                    $('.js_loader_x__content').addClass('show')
                }, 200)
                setTimeout(() => {
                    removeLoader()
                }, 1500);

            }
        });
    }




    $(".js_animate__move_top").each(function () {
        let el = $(this)[0];
        let elParent = $(this).parent()[0];
        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0;

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 95%',
            },
            opacity: 1,
            y: '0',
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            duration: 1

        })
    })



    $(".js_title_animate").each(function () {
        el = $(this).find('span')
        elParent = $(this).parent()[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 95%',
            },
            y: '0',
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            duration: 1

        })
    })

    $(".js_animate__opacity").each(function () {
        el = $(this)[0];

        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0;
        let duration = $(this).data('duration') ? $(this).data('duration') : 1;

        gsap.to(el, {
            scrollTrigger: {
                trigger: el,
                start: 'top 95%',
            },
            opacity: 1,
            delay: delayTime,
            duration: duration

        })
    })


    $(".js_animate__to_scale").each(function () {
        el = $(this)[0];

        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0;
        let duration = $(this).data('duration') ? $(this).data('duration') : 1;

        gsap.to(el, {
            scrollTrigger: {
                trigger: el,
                start: 'top 85%',
            },
            scale: 1,
            delay: delayTime,
            duration: duration

        })
    })

    $(".js_animate__from_scale").each(function () {
        el = $(this)[0];

        let delayTime = $(this).data('delay') ? $(this).data('delay') : 0;
        let duration = $(this).data('duration') ? $(this).data('duration') : 1;

        gsap.to(el, {
            scrollTrigger: {
                trigger: el,
                start: 'top 75%',
            },
            scale: 1,
            delay: delayTime,
            duration: duration

        })
    })

    $(".js_animate__image__view").each(function () {
        el = $(this).find('.js_animate__move__white_line')
        elParent = $(this).parent()[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 85%',
            },
            scaleY: '0',
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            duration: 1.25

        })
    })

    $('body').mousemove(function (e) {
        $('.js_move').each(function (index, element) {
            parallaxIt(e, $(this), $(this).data('speed'), $(this).data('delay'))
        });
    });


    function parallaxIt(e, target, movement = 40, delayTime = 0) {
        var $this = $('body');

        gsap.to(target, {
            x: (e.clientX - $this.width() / 1.5) / $this.width() * movement,
            y: (e.clientY - $this.width() / 1.5) / $this.width() * movement,
            ease: "ease-in-out",
            delay: delayTime,
            duration: 1.75
        })
    }






    if (localStorage.getItem('preloader') != '1') {
        $('.js_loader_x').css('display', 'flex')
        loadedPreloader()
    }else{
        animateAfterLoaded(500);
    }
});