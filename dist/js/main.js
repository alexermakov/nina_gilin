function getBodyScrollTop() {
	return self.pageYOffset || (document.documentElement && document.documentElement.ScrollTop) || (document.body && document.body.scrollTop);
}
$(function () {

	let lastScrollTop = 0;
	$(window).scroll(function () {
		const $header = $('.js_header');
		let st = $(this).scrollTop();



		let addClassHide = st > lastScrollTop ? true : false;

		if (st < $(window).height() / 2) {
			addClassHide = false;
		}


		if (addClassHide) {
			$header.addClass('header_hide')
		} else {
			$header.removeClass('header_hide')
		}
		lastScrollTop = st;
	});

	$(window).on("load", function () {
		$('body').addClass('loaded')
	})

	Fancybox.bind('.js__modal', {
		autoFocus: false,
		trapFocus: false,
		touch: false,
		closeButton: 'outside',
	});


	$(".js_select").each(function () {
		let placeholder = $(this).attr('placeholder');
		$(this).select2({
			minimumResultsForSearch: 1 / 0,
			placeholder: placeholder,
			// allowClear: true
		})
	});


	$('.js__btn_close__modal').click(function (e) {
		Fancybox.close();
	});

	$('.js_modal__wishlist__block__show__all').click(function (e) {
		$(this).hide(0)
		$('.js_modal__wishlist__list .modal__wishlist__list__item').each(function () {
			if ($(this).is(":hidden")) $(this).slideDown(400)
		})
	});





	$('.js__btn_open__modal').click(function (e) {
		Fancybox.show([{
			src: "#js_modal__call"
		}])
	});


	$('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu_overlay').click(function (e) {
		e.preventDefault();

		$('.js_btn_menu').toggleClass('active')
		$('.js__modal__menu').toggleClass('active')
		$('body').toggleClass('open_menu')
		$('body').toggleClass('body-lock');
		$('.js_modal__menu_overlay').toggleClass('active')



	});



	$('.js_form__contact').submit(function (event) {
		if ($(this)[0].checkValidity()) {
			$('.js_contact__thanks').fadeIn(400)

		}
	});

	$('.js_form__reset__password').submit(function (event) {
		if ($(this)[0].checkValidity()) {
			$('.js_modal__myaccount--reset_pasword.active').removeClass('active')
			$('.js_modal__myaccount--thanks').addClass('active')

		}
	});

	Fancybox.defaults.Thumbs = false;
	Fancybox.defaults.Toolbar = false;


	$('.js_about__section_gallery__list').slick({
		slidesToShow: 3,
		dots: false,
		responsive: [{
			breakpoint: 749,
			settings: {
				variableWidth: true,
				slidesToShow: 1,
			}
		}, ]

	})

	$('.js_home__product__slider__2').slick({
		slidesToShow: 1,
		dots: true,
		arrows: false,
		asNavFor: '.js_home__product__slider',
	})

	$('.js_home__product__slider').slick({
		slidesToShow: 1,
		dots: false,
		arrows: false,
		fade: true,
		asNavFor: '.js_home__product__slider__2',
		cssEase: 'linear'
	})


	$('.js_collection__home__slider_1').slick({
		slidesToShow: 1,
		dots: true,
		arrows: false,
		asNavFor: '.js_collection__home__slider_2',
		autoplay: true,
		speed: 1000
	})

	$('.js_collection__home__slider_2').slick({
		slidesToShow: 1,
		dots: false,
		arrows: false,
		asNavFor: '.js_collection__home__slider_1',
		autoplay: true,
		speed: 1000
	})






	$('.js_modal__has__sub__menu').each(function (index, element) {
		$(this).append(`<div class='js_btn_modal__has__sub__menu btn_modal__has__sub__menu'>open/close</div>`)
	});

	$(document).on('click', '.js_btn_modal__has__sub__menu', function (e) {
		$(this).closest('.js_modal__has__sub__menu').first().toggleClass('active')
		$(this).closest('.js_modal__has__sub__menu').first().find('.modal__sub__menu').first().slideToggle(400)
	})


	$('.js__btn_show__modal').click(function (e) {
		e.preventDefault();
		$('.js_modal__myaccount.active').removeClass('active')
		let $classModal = $(this).data('modal');
		$('.' + $classModal).addClass('active')
	})

	$('.js__btn__modal__myaccount__close').click(function (e) {
		e.preventDefault();
		$(this).closest('.js_modal__myaccount').removeClass('active');
	})

	$('.js_form_x__item__password__icon').click(function (e) {
		e.preventDefault();
		let $input = $(this).closest('.js_form_x__item--password').find('input');
		$(this).toggleClass('active');
		if ($input.attr('type') == 'password') {
			$input.attr('type', 'text')
		} else {
			$input.attr('type', 'password')
		}
	})


	$('.js_modal__myaccount').click(function (e) {
		if (!$(e.target).closest('.modal__myaccount__block').length) {
			$(this).removeClass('active');
		}
	})

	if ($('.js_price_filter_min').length){

		const inputsSliderPrice = [document.querySelector('.js_price_filter_min'), document.querySelector('.js_price_filter_max')];
		const priceSliders = document.querySelector('.js_price_slider');


		let min = $(priceSliders).data('min');
		let max = $(priceSliders).data('max');
		noUiSlider.create(priceSliders, {
			start: [min, max],
			connect: true,
			step:1,
			format: wNumb({
				decimals: 0
			}),
			range: {
				'min': min,
				'max': max
			}
		});

		priceSliders.noUiSlider.on('update', function (values, handle) {
			inputsSliderPrice[handle].value = values[handle];
		});

		inputsSliderPrice.forEach(function (input, handle) {
			input.addEventListener('keyup', function () {
				priceSliders.noUiSlider.setHandle(handle, this.value);
			});
		})


		$(document).on('click', function (e) {
			let clickedElement = $(e.target);
			if (!(clickedElement.hasClass('js_cat_fil__attribute') || clickedElement.closest('.js_cat_fil__attribute').length || clickedElement.hasClass('js_clear_cat_fil__attribute'))){
				$('.js_cat_fil__attribute').removeClass('opened');
			}
		});

		$('.js_cat_fil__attribute .cat_fil__attribute__view').click(function(){
			let elParent = $(this).closest('.js_cat_fil__attribute')
			let opened = elParent.hasClass('opened');
			$('.js_cat_fil__attribute').removeClass('opened');

			if (!opened){
				elParent.addClass('opened')
			}
		})

		$('.js_btn__cat_fil__apply').click(function(){
			let elParent = $(this).closest('.js_cat_fil__attribute')
			elParent.removeClass('opened')
		})

		$('.js_btn__cat_color').click(function(){
			let elParent = $(this).closest('.js_cat_fil__attribute')
			let elParentTitle = elParent.find('.cat_fil__attribute__view__current')
			let count = elParent.find('.js_cat_fil__attribute__color__list input:checked').length;
			if (count >0){
				let countTextArray = elParentTitle.attr('data-before').split(',')
				let countText = count==1 ? countTextArray[0] : countTextArray[1];
				elParentTitle.text(`${countText} ${count}`)
				elParent.addClass('choosed');
				elParentTitle.after(`<span class='clear_cat_fil__attribute js_clear_cat_fil__attribute' data-attribute='color'></span>`)
			}else{
				elParent.removeClass('choosed');
				let defaultText = elParentTitle.data('default');
				elParentTitle.text(defaultText)
			}
		})


		$('.js_btn__cat_price').click(function(){
			let elParent = $(this).closest('.js_cat_fil__attribute')
			let elParentTitle = elParent.find('.cat_fil__attribute__view__current')
			let $minPrice = elParent.find('.js_price_filter_min');
			let $maxPrice = elParent.find('.js_price_filter_max');
			if ($minPrice.val()!=$minPrice.data('default')  || $maxPrice.val() != $maxPrice.data('default')){
				let countText = elParentTitle.attr('data-before');
				elParentTitle.text(`${countText} ${$minPrice.val()} - ${$maxPrice.val() }`)
				elParent.addClass('choosed');
				elParentTitle.after(`<span class='clear_cat_fil__attribute js_clear_cat_fil__attribute' data-attribute='price'></span>`)
			}else{
				elParent.removeClass('choosed');
				let defaultText = elParentTitle.data('default');
				elParentTitle.text(defaultText)
			}
		})
	}

	$(document).on('click','.js_clear_cat_fil__attribute',function(){

		let typeAttirbute = $(this).attr('data-attribute');
		let elParent = $(this).closest('.js_cat_fil__attribute');
		let elParentSection = elParent.find('.cat_fil__attribute__content');
		let elParentTitle = elParent.find('.cat_fil__attribute__view__current')

		if (typeAttirbute=='color'){
			elParentSection.find('.js_cat_fil__attribute__color__list input').each(function (index, element) {
				$(this).prop('checked', false);
			});
		}

		if (typeAttirbute=='price'){
			let $priceElMin = elParentSection.find('.js_price_filter_min')
			let $priceElMax = elParentSection.find('.js_price_filter_max')
			$priceElMin.val($priceElMin.data('default'))
			$priceElMax.val($priceElMax.data('default'))

			const priceSliders = document.querySelector('.js_price_slider');

			priceSliders.noUiSlider.set([$priceElMin.data('default'),$priceElMax.data('default')]);
		}

		elParent.removeClass('choosed');
		let defaultText = elParentTitle.data('default');
		elParentTitle.text(defaultText)
		$(this).remove()

	})

	$('.js_mega__menu__type__list .mega__menu__type__link').hover(function () {
		$('.js_mega__menu__type__list .mega__menu__type__link').removeClass('active')
			$(this).addClass('active')
		}
	);











});